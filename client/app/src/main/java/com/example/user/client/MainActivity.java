package com.example.user.client;
import java.net.UnknownHostException;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.ContextMenu.*;
import android.widget.ArrayAdapter;
import android.widget.*;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.ExecutionException;
import  java.lang.InterruptedException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.InetAddress;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
     //private  TextView mtextView;
     private ListView mListView;
    public static final int IDM_DELETE = 101;
    public static final int IDM_COPY = 102;
    public static final int IDM_RENNAME = 103;
    public static final int IDM_PASTE= 104;
    public static final int IDM_MOVE= 105;
    public static final int IDM_CREATE= 107;
    //public static final int IDM_SHUTDOWN= 106;

    final Context context = this;
    BlockingQueue<String> drop = new SynchronousQueue<String>();
    ArrayList<String> mList = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.ListView);
      //  mtextView = (TextView) findViewById(R.id.textView);

        registerForContextMenu(mListView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,long id) {
                try {
                    if (position != 0) {
                        TextView textView = (TextView) itemClicked;
                        drop.put("cleak");

                        drop.put(textView.getText().toString());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu1, View v, ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu1, v, menuInfo);

        menu1.add(Menu.NONE, IDM_DELETE, Menu.NONE, "Удалить");
        menu1.add(Menu.NONE, IDM_MOVE, Menu.NONE, "Переместить");
        menu1.add(Menu.NONE, IDM_COPY, Menu.NONE, "Копировать");
        menu1.add(Menu.NONE, IDM_PASTE, Menu.NONE, "Вставить");
        menu1.add(Menu.NONE, IDM_RENNAME, Menu.NONE, "Переименовать");
        menu1.add(Menu.NONE, IDM_CREATE, Menu.NONE, "Создать папку");
      //  menu1.add(Menu.NONE, IDM_SHUTDOWN, Menu.NONE, "Выключить копьютер");
        System.out.println("2");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.conect:

                AlertDialog.Builder alert = new AlertDialog.Builder(this);

                alert.setTitle("Введите IP");


// Set an EditText view to get user input
                final EditText input = new EditText(this);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = String.valueOf(input.getText());
                        if(checkIP(value)) {
                            conect(value);
                        }
                        else{
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Введеное сообшение не является IP", Toast.LENGTH_SHORT);
                            toast.show();
                        };
                        // Do something with value!
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();


                return true;


            default:
                return true;
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        CharSequence message;
        switch (item.getItemId())
        {
            case IDM_DELETE:
                try {
                    drop.put("DELETE");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Нажмите на элемент который котите удалить", Toast.LENGTH_SHORT);
                toast.show();
               // message = "Выбран пункт уДАЛИТЬ";
                break;
            case IDM_MOVE:
                try {
                    drop.put("MOVE");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toast = Toast.makeText(getApplicationContext(),
                        "Нажмите на элемент который котите сохранить себе на устройство", Toast.LENGTH_SHORT);
                toast.show();
                // message = "Выбран пункт Сохранить";
                break;
            case IDM_COPY:
                try {
                    drop.put("COPY");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toast = Toast.makeText(getApplicationContext(),
                        "Нажмите на элемент который котите сохранить себе на устройство", Toast.LENGTH_SHORT);
                toast.show();
               // message = "Выбран пункт Сохранить";
                break;

            case IDM_PASTE:
                try {
                    drop.put("PASTE");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // message = "Выбран пункт Сохранить";
                break;
            case  IDM_RENNAME:

                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Введите новое имя");


// Set an EditText view to get user input
                final EditText input = new EditText(this);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = String.valueOf(input.getText());
                        if(checkName(value)){
                           Toast toast = Toast.makeText(getApplicationContext(),
                                    "Нажмите на элемент который котите переименовть ", Toast.LENGTH_SHORT);
                            toast.show();
                            try {
                                drop.put("RENNAME");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            try {
                                drop.put(value);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                             Toast toast = Toast.makeText(getApplicationContext(),
                                    "Недопустимые символы", Toast.LENGTH_SHORT);
                            toast.show();
                        }


                        // Do something with value!
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
                break;
            case  IDM_CREATE:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                alert1.setTitle("Введите название папки");


// Set an EditText view to get user input
                final EditText input1 = new EditText(this);
                alert1.setView(input1);

                alert1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = String.valueOf(input1.getText());
                        if(checkName(value)){

                            try {
                                drop.put("CREATE");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            try {
                                drop.put(value);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Недопустимые символы", Toast.LENGTH_SHORT);
                            toast.show();
                        }


                        // Do something with value!
                    }
                });

                alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert1.show();
                break;


            default:
                return super.onContextItemSelected(item);
        }




        return true;
    }
    public void conect(String IP){
         // mList.add("adasdad");
                ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mList);
                 mListView.setAdapter(adapter);
                CLIENT client = new CLIENT();
                 System.out.println(IP);
                try {
                    InetAddress ipAddress = InetAddress.getByName(IP);
                    client.setAddress(ipAddress);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                 System.out.println(IP);

                client.setListView(mListView,adapter);

                client.execute(drop);

    }

    private  boolean checkName(String name){

        for(int i=0;i<name.length();i++){
            if(name.charAt(i)=='~'&&name.charAt(i)=='#'&&name.charAt(i)=='&'&&name.charAt(i)=='*'&&name.charAt(i)=='{'&&name.charAt(i)=='}'&&
                    name.charAt(i)=='\\' &&  name.charAt(i)==':'&& name.charAt(i)=='<'&&
                    name.charAt(i)=='>'&& name.charAt(i)=='?'&& name.charAt(i)=='/'&& name.charAt(i)=='+'&&
                    name.charAt(i)=='!'&& name.charAt(i)=='"') return  false;
        }
        return true;
    }


    private  boolean checkIP(String ip){
        if(ip.length()>15)return  false;
        if(!Character.isDigit(ip.charAt(0)))return false;
        for(int i=1;i<ip.length()-1;i++){
            if(ip.charAt(i)=='.'&& ip.charAt(i+1)=='.')return  false;
        }
        for(int i=0;i<ip.length();i++){
            if(!Character.isDigit(ip.charAt(i))  &&  ip.charAt(i)!='.' ) return  false;
        }
        return true;
    }
   /* private  boolean checktime(String time){

        for(int i=1;i<time.length()-1;i++){
            if(!Character.isDigit(time.charAt(0)))return  false;
        }

        return true;
    }*/

}
